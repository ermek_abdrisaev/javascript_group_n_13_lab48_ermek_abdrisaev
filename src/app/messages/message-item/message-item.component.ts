import { Component, Input, OnInit } from '@angular/core';
import { Message } from '../../shared/message.model';
import { MessagesService } from '../../shared/messages.service';

@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.css']
})
export class MessageItemComponent implements OnInit {
  @Input() message!: Message;
  @Input() time!: string;

  constructor(private messagesService: MessagesService) { }

  ngOnInit(): void {

  }

}
