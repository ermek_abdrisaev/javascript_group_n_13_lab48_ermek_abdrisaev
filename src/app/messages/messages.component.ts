import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../shared/message.model';
import { MessagesService } from '../shared/messages.service';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messages!: Message[];
  timeArr: string[] = [];
  messagesChangeSubscription!: Subscription;
  messagesFetchingSubscription!: Subscription;
  loading: boolean = false;

  constructor(private messagesService: MessagesService) { }

  ngOnInit(): void {
    this.messagesChangeSubscription = this.messagesService.messageChange.subscribe((messages: Message[]) =>{
      this.messages = messages.reverse();
      this.timeArr = this.messages.map( message => {
        return new Date(message.datetime).toDateString() + ' ' +  new Date(message.datetime).toLocaleTimeString();
      })
    });
    this.messagesFetchingSubscription = this.messagesService.messagesFetching.subscribe((isFetching: boolean) =>{
      this.loading = isFetching;
    });
    this.messagesService.fetchMessages();

  }

  ngOnDestroy(){
    this.messagesChangeSubscription.unsubscribe();
    this.messagesFetchingSubscription.unsubscribe();
  }
}
