import { HttpClient, HttpParams } from '@angular/common/http';
import { Message } from './message.model';
import {  Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';

@Injectable()

export class MessagesService {
  messageChange = new Subject<Message[]>();
  messagesFetching = new Subject<boolean>();
  messageUploading = new Subject<boolean>();

  constructor(private http: HttpClient, private route: ActivatedRoute){}
  private messages: Message[] = [];

  fetchMessages(){
    this.messagesFetching.next(true);

    this.http.get<{[id: string]: Message}>('http://146.185.154.90:8000/messages')
      .pipe(map(result =>{
        return Object.keys(result).map(id =>{
          const messageData = result[id];
          return new Message(id, messageData.author, messageData.message, messageData.datetime);
        });
      }))
      .subscribe(messages =>{
        this.messages = messages;
        this.messageChange.next(this.messages.slice());
        this.messagesFetching.next(false);
      });
  }

  getMessages(){
    return this.messages.slice();
  }

  addMessage(message: HttpParams){
    this.messageUploading.next(true);
    return this.http.post('http://146.185.154.90:8000/messages', message)
      .pipe(tap(() =>{
      this.messageUploading.next(false);
    }, () =>{
      this.messageUploading.next(false);
    })
    );
  }

}
