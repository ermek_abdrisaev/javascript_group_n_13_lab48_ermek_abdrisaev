import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MessagesService } from '../shared/messages.service';
import { Subscription } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css']
})
export class NewMessageComponent implements OnInit, OnDestroy {
  @ViewChild('m') messageForm!: NgForm;
  messageUploadingSubscription!: Subscription;
  isUploading = false;

  constructor(
    private messageService: MessagesService) { }

  ngOnInit(): void {
    this.messageUploadingSubscription = this.messageService.messageUploading.subscribe((isUploading: boolean) =>{
      this.isUploading = isUploading;
    });
  }

  createMessage(){
    const body = new HttpParams()
      .set('author', this.messageForm.value.author)
      .set('message', this.messageForm.value.message);
    this.messageService.addMessage(body).subscribe();
    this.messageService.fetchMessages();
  }

  ngOnDestroy(): void{
    this.messageUploadingSubscription.unsubscribe();
  }
}
